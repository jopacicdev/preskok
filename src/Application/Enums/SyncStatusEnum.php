<?php declare(strict_types=1);

namespace Preskok\Application\Enums;

class SyncStatusEnum
{
    const DISK = 'DISK';
    const SYNC = 'SYNC';
    const AWS = 'AWS';
}
