<?php declare(strict_types=1);

namespace Preskok\Application\Contracts;

interface ValidatorInterface
{
    public function isValid($value): bool;
}
