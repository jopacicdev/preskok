<?php declare(strict_types=1);

namespace Preskok\Application\Contracts;

interface RunnableServiceInterface
{
    /**
     * Makes service runnable
     */
    public function run(): void;
}
