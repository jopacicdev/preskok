<?php declare(strict_types=1);

namespace Preskok\Application\DTO;

class File
{
    /**
     * @var string
     */
    public $filename;
    /**
     * @var string
     */
    public $contents;

    public function __construct(string $filename, string $contents)
    {
        $this->filename = $filename;
        $this->contents = $contents;
    }
}
