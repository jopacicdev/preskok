<?php declare(strict_types=1);

namespace Preskok\Application\DTO;

class Record
{
    /**
     * @var int
     */
    public $vehicleId;
    /**
     * @var int
     */
    public $inhouseSellerId;
    /**
     * @var int
     */
    public $buyerId;
    /**
     * @var int
     */
    public $modelId;
    /**
     * @var string
     */
    public $saleDate;
    /**
     * @var string
     */
    public $buyDate;

    public function __construct(
        int $vehicleId,
        int $inhouseSellerId,
        int $buyerId,
        int $modelId,
        string $saleDate,
        string$buyDate
    ) {
        $this->vehicleId = $vehicleId;
        $this->inhouseSellerId = $inhouseSellerId;
        $this->buyerId = $buyerId;
        $this->modelId = $modelId;
        $this->saleDate = $saleDate;
        $this->buyDate = $buyDate;
    }
}
