<?php declare(strict_types=1);

namespace Preskok\Application;

use Psr\Container\ContainerInterface;
use Symfony\Component\Console\Application as SymfonyConsoleApplication;

class Application extends SymfonyConsoleApplication
{
    /** @var ContainerInterface */
    private $container;

    public function __construct(ContainerInterface $container, string $name = 'UNKNOWN', string $version = 'UNKNOWN')
    {
        $this->container = $container;

        parent::__construct($name, $version);
    }

    public function getContainer(): ContainerInterface
    {
        return $this->container;
    }
}
