<?php declare(strict_types=1);

namespace Preskok\Application\Services;

use Preskok\Application\Exceptions\DatabaseErrorException;
use Preskok\Application\Repositories\ReportsRepository;

class ReportsService
{
    /**
     * @var ReportsRepository
     */
    private $reportsRepository;

    public function __construct(ReportsRepository $reportsRepository)
    {
        $this->reportsRepository = $reportsRepository;
    }

    /**
     * @return array
     *
     * @throws DatabaseErrorException
     */
    public function getBestSellingModelPerClient(): array
    {
        return $this->reportsRepository->getBestSellingModelPerClient();
    }

    /**
     * @return array
     *
     * @throws DatabaseErrorException
     */
    public function getBestSellingModelThreeMonthsPerClient(): array
    {
        return $this->reportsRepository->getBestSellingModelThreeMonthsPerClient();
    }
}
