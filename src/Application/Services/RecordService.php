<?php declare(strict_types=1);

namespace Preskok\Application\Services;

use Preskok\Application\DTO\Record;
use Preskok\Application\Exceptions\DatabaseErrorException;
use Preskok\Application\Repositories\RecordRepository;

class RecordService
{
    /**
     * @var RecordRepository
     */
    private $recordRepository;

    public function __construct(RecordRepository $recordRepository)
    {
        $this->recordRepository = $recordRepository;
    }

    /**
     * @param array $inputData
     *
     * @return bool
     *
     * @throws DatabaseErrorException
     */
    public function add(array $inputData): bool
    {
        $record = new Record(
            $inputData['VehicleID'],
            $inputData['InhouseSellerID'],
            $inputData['BuyerID'],
            $inputData['ModelID'],
            $inputData['SaleDate'],
            $inputData['BuyDate']
        );

        return $this->recordRepository->add($record);
    }
}
