<?php declare(strict_types=1);

namespace Preskok\Application\Exceptions;

use Exception;
use Throwable;

class DatabaseErrorException extends Exception
{
    public function __construct(array $pdoErrors = [], int $code = 0, Throwable $previous = null)
    {
        $lastError = end($pdoErrors);
        $message = 'Error running query: ' . $lastError;

        parent::__construct($message, $code, $previous);
    }
}
