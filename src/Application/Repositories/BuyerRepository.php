<?php declare(strict_types=1);

namespace Preskok\Application\Repositories;

use Faker\Generator;
use PDO;

class BuyerRepository
{
    /**
     * @var PDO
     */
    private $db;
    /**
     * @var Generator
     */
    private $faker;

    /**
     * BuyerRepository constructor.
     *
     * @param PDO $db
     * @param Generator $faker
     */
    public function __construct(PDO $db, Generator $faker)
    {
        $this->db = $db;
        $this->faker = $faker;
    }

    public function generateFakeBuyers(array $buyerIdsList)
    {
        $query = "REPLACE INTO buyers (ID, FirstName, LastName) VALUES (:ID, :FirstName, :LastName)";

        foreach ($buyerIdsList as $id) {
            $firstName = $this->faker->firstName;
            $lastName = $this->faker->lastName;

            $stmt = $this->db->prepare($query);
            $stmt->execute([
                ':ID' => $id,
                ':FirstName' => $firstName,
                ':LastName' => $lastName,
            ]);
        }
    }
}
