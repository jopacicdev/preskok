<?php declare(strict_types=1);

namespace Preskok\Application\Repositories;

use PDO;
use Preskok\Application\Exceptions\DatabaseErrorException;

class ReportsRepository
{
    /**
     * @var PDO
     */
    private $db;

    public function __construct(PDO $db)
    {
        $this->db = $db;
    }

    /**
     * @return array
     *
     * @throws DatabaseErrorException
     */
    public function getBestSellingModelPerClient(): array
    {
        $query = "
          SELECT 
            BuyerId, 
            CONCAT(FirstName, ' ', LastName) AS Name, 
            ModelId, 
            MAX(ModelSaleOccurences) AS ModelSales
          FROM (
              SELECT COUNT(ModelId) AS ModelSaleOccurences, ModelId, BuyerId 
              FROM records
              GROUP BY BuyerId, ModelId 
              ORDER BY ModelSaleOccurences DESC
          ) AS CountedRecords 
          INNER JOIN buyers
          ON CountedRecords.BuyerId = buyers.ID
          GROUP BY BuyerId
        ";

        $stmt = $this->db->prepare($query);
        if (!$stmt->execute()) {
            throw new DatabaseErrorException($stmt->errorInfo());
        }

        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }

    /**
     * @return array
     *
     * @throws DatabaseErrorException
     */
    public function getBestSellingModelThreeMonthsPerClient(): array
    {
        $query = "
          SELECT t.ModelID,
           MONTHNAME(p.b),
           MONTHNAME(p.e),
           COUNT(*) as SaleCount
           FROM records t
                INNER JOIN (SELECT DISTINCT
                                   date_add(SaleDate, INTERVAL -day(SaleDate) + 1 DAY) b,
                                   date_add(date_add(SaleDate, INTERVAL -day(SaleDate) + 1 DAY), INTERVAL 3 MONTH) e
                                   FROM records
                            UNION
                            SELECT DISTINCT
                                   date_add(date_add(SaleDate, INTERVAL -day(SaleDate) + 1 DAY), INTERVAL -3 MONTH) b,
                                   date_add(SaleDate, INTERVAL -day(SaleDate) + 1 DAY) e
                                   FROM records) p
                            ON p.b <= t.SaleDate
                               AND p.e > t.SaleDate
           GROUP BY t.ModelID,
                    p.b,
                    p.e
           ORDER BY COUNT(*) DESC
           LIMIT 1;
        ";

        $stmt = $this->db->prepare($query);
        if (!$stmt->execute()) {
            throw new DatabaseErrorException($stmt->errorInfo());
        }

        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }
}
