<?php declare(strict_types=1);

namespace Preskok\Application\Repositories;

use Preskok\Application\DTO\File;
use RuntimeException;

class StorageRepository
{
    const STORAGE_DIR = BASE_DIR . '/storage/';
    const STORAGE_DIR_WIN = BASE_DIR . '\\storage\\';

    public function getLocalFiles($directory)
    {
        $data = scandir(($this->isWindows() ? self::STORAGE_DIR_WIN : self::STORAGE_DIR) . $directory);
        $files = array_diff($data, ['.', '..']);

        return $files;
    }

    /**
     * @param string $file
     * @param string $directory
     *
     * @return File
     */
    public function getLocalFileDTO(string $file, string $directory): File
    {
        $file = ($this->isWindows()) ? '\\' . $file : '/' . $file;
        $filename = ($this->isWindows() ? self::STORAGE_DIR_WIN : self::STORAGE_DIR) . $directory . $file;
        $contents = file_get_contents($filename);

        if ($contents === false) {
            throw new RuntimeException('Cannot get contents for ' . $filename);
        }

        return new File($filename, $contents);
    }

    public function save($file, $contents, $directory)
    {
        $file = ($this->isWindows()) ? '\\' . $file : '/' . $file;
        $filename = ($this->isWindows() ? self::STORAGE_DIR_WIN : self::STORAGE_DIR) . $directory . $file;
        if (false === file_put_contents($filename, $contents)) {
            throw new RuntimeException('Could not save ' . $filename);
        }

        return $filename;
    }

    /**
     * @return bool
     */
    protected function isWindows(): bool
    {
        return strtoupper(substr(PHP_OS, 0, 3)) === 'WIN';
    }
}
