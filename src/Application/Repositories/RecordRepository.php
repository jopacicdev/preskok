<?php declare(strict_types=1);

namespace Preskok\Application\Repositories;

use PDO;
use Preskok\Application\DTO\Record;
use Preskok\Application\Exceptions\DatabaseErrorException;

class RecordRepository
{
    /**
     * @var PDO
     */
    private $db;

    /**
     * RecordRepository constructor.
     *
     * @param PDO $db
     */
    public function __construct(PDO $db)
    {
        $this->db = $db;
    }

    /**
     * @param Record $record
     *
     * @return bool
     *
     * @throws DatabaseErrorException
     */
    public function add(Record $record): bool
    {
        $query = "
          INSERT INTO records (
              VehicleID, 
              InhouseSellerID, 
              BuyerID, 
              ModelID, 
              SaleDate, 
              BuyDate
          ) VALUES (
            :vehicleId,
            :inhouseSellerId,
            :buyerId,
            :modelId,
            :saleDate,
            :buyDate
          )";

        $stmt = $this->db->prepare($query);
        $result = $stmt->execute(
            [
                'vehicleId' => $record->vehicleId,
                'inhouseSellerId' => $record->inhouseSellerId,
                'buyerId' => $record->buyerId,
                'modelId' => $record->modelId,
                'saleDate' => $record->saleDate,
                'buyDate' => $record->buyDate,
            ]
        );

        if (!$result) {
            throw new DatabaseErrorException('Could not add record to database!');
        }

        return (bool)$result;
    }

    public function getAggregatedListOfBuyerIds(): array
    {
        $query = "SELECT DISTINCT BuyerID FROM `records`";

        $stmt = $this->db->prepare($query);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_COLUMN, 0);

        return array_map('intval', $result);
    }
}
