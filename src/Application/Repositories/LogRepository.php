<?php declare(strict_types=1);

namespace Preskok\Application\Repositories;

use PDO;
use Preskok\Application\Enums\SyncStatusEnum;

class LogRepository
{
    /**
     * @var PDO
     */
    private $db;

    public function __construct(PDO $db)
    {
        $this->db = $db;
    }

    public function getLocalVersionedFiles(): array
    {
        $query = "SELECT file FROM log WHERE status = :status";

        $stmt = $this->db->prepare($query);
        $stmt->execute(['status' => SyncStatusEnum::DISK]);
        $result = $stmt->fetchAll(PDO::FETCH_COLUMN, 0);

        return $result;
    }

    public function getRemoteVersionedFiles(): array
    {
        $query = "SELECT file FROM log WHERE status = :status";

        $stmt = $this->db->prepare($query);
        $stmt->execute(['status' => SyncStatusEnum::AWS]);
        $result = $stmt->fetchAll(PDO::FETCH_COLUMN, 0);

        return $result;
    }

    public function addLocalFiles(array $files): void
    {
        $this->addFiles($files, SyncStatusEnum::DISK);
    }

    public function addRemoteFiles(array $files): void
    {
        $this->addFiles($files, SyncStatusEnum::AWS);
    }

    public function addFiles(array $files, string $status = SyncStatusEnum::SYNC): void
    {
        foreach ($files as $file) {
            $query = "INSERT INTO log (file, status) VALUES (:file, :status)";

            $statement = $this->db->prepare($query);
            $statement->execute(
                [
                    'file' => $file,
                    'status' => $status
                ]
            );
        }
    }

    public function setFileToSynced(string $file): void
    {
        $query = "UPDATE log SET status = :status WHERE file = :file";

        $stmt = $this->db->prepare($query);
        $stmt->execute(
            [
                'status' => SyncStatusEnum::SYNC,
                'file' => $file,
            ]
        );
    }
}
