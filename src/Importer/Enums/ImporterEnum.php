<?php declare(strict_types=1);

namespace Preskok\Importer\Enums;

class ImporterEnum
{
    const COMMAND_NAME = 'Import';
    const COMMAND_DESCRIPTION = 'Import data from remote location.';

    const COMMAND_ARGUMENT_URL = 'url';
    const COMMAND_ARGUMENT_DESCRIPTION = 'Remote location URL';
    const INVALID_ARGUMENT_EXCEPTION_MESSAGE = 'The given location URL is not valid!';

    const CHUNK_SIZE = 250;
}
