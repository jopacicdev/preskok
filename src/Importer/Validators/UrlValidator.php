<?php declare(strict_types=1);

namespace Preskok\Importer\Validators;

use Preskok\Application\Contracts\ValidatorInterface;

class UrlValidator implements ValidatorInterface
{
    public function isValid($value): bool
    {
        if (strpos($value, "http://") !== 0 && strpos($value, "https://") !== 0) {
            return false;
        }

        if (filter_var(
            $value,
            FILTER_VALIDATE_URL,
            FILTER_FLAG_SCHEME_REQUIRED | FILTER_FLAG_HOST_REQUIRED
        ) === false) {
            return false;
        }

        return true;
    }
}
