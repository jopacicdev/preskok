<?php declare(strict_types=1);

namespace Preskok\Importer\Services;

use Preskok\Importer\Repositories\ImporterRepository;

class ImporterService
{
    /**
     * @var ImporterRepository
     */
    private $repository;

    public function __construct(ImporterRepository $repository)
    {
        $this->repository = $repository;
    }

    public function import($url)
    {
        $data = $this->repository->fetchRemoteData($url);
        $this->repository->import($data);

        return count($data);
    }
}
