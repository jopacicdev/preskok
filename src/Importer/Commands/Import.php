<?php declare(strict_types=1);

namespace Preskok\Importer\Commands;

use InvalidArgumentException;
use Preskok\Importer\Enums\ImporterEnum;
use Preskok\Importer\Services\ImporterService;
use Preskok\Importer\Validators\UrlValidator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Import extends Command
{
    protected function configure()
    {
        $this
            ->setName(ImporterEnum::COMMAND_NAME)
            ->setDescription(ImporterEnum::COMMAND_DESCRIPTION)
            ->addArgument(
                ImporterEnum::COMMAND_ARGUMENT_URL,
                InputArgument::REQUIRED,
                ImporterEnum::COMMAND_ARGUMENT_DESCRIPTION
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $url = $input->getArgument('url');
        $this->validate($url);

        $output->writeln("<info>Importing data...</info>");
        /** @var ImporterService $importerService */
        $importerService = $this->getApplication()->getContainer()->get(ImporterService::class);
        $count = $importerService->import($url);

        $output->writeln("<info>Total of $count rows imported</info>");
    }

    /**
     * @param $url
     */
    protected function validate($url): void
    {
        /** @var UrlValidator $validator */
        $validator = $this->getApplication()->getContainer()->get(UrlValidator::class);

        if (!$validator->isValid($url)) {
            throw new InvalidArgumentException(ImporterEnum::INVALID_ARGUMENT_EXCEPTION_MESSAGE);
        }
    }
}
