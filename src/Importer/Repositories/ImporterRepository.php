<?php declare(strict_types=1);

namespace Preskok\Importer\Repositories;

use PDO;
use Preskok\Importer\Enums\ImporterEnum;

class ImporterRepository
{
    /**
     * @var PDO
     */
    private $db;

    public function __construct(PDO $db)
    {
        $this->db = $db;
    }

    /**
     * @param string $url
     *
     * @return array
     */
    public function fetchRemoteData(string $url): array
    {
        $rows = array_map('str_getcsv', file($url));

        array_walk($rows, function (&$row) use ($rows) {
            $row = array_combine($rows[0], $row);
            $row['VehicleID'] = trim(str_replace('<br>', '', $row['VehicleID']));
        });

        array_shift($rows);

        return $rows;
    }

    /**
     * @param array $data
     */
    public function import(array $data)
    {
        $chunks = array_chunk($data, ImporterEnum::CHUNK_SIZE);

        foreach ($chunks as $chunk) {
            $query = "INSERT INTO records (VehicleID, InhouseSellerID, BuyerID, ModelID, SaleDate, BuyDate) VALUES ";

            foreach ($chunk as $index => $row) {
                $query .= sprintf(
                    "(%s, %s, %s, %s, '%s', '%s')%s " . PHP_EOL,
                    $row['VehicleID'],
                    $row['InhouseSellerID'],
                    $row['BuyerID'],
                    $row['ModelID'],
                    $row['SaleDate'],
                    $row['BuyDate'],
                    $index + 1 != count($chunk) ? ',' : ''
                );
            }

            $this->db->exec($query);
        }
    }
}
