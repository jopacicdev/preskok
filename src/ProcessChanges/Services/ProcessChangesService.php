<?php declare(strict_types=1);

namespace Preskok\ProcessChanges\Services;

use DateTimeImmutable;
use Exception;
use Preskok\ProcessChanges\Repositories\ChangesRepository;

class ProcessChangesService
{
    /**
     * @var ChangesRepository
     */
    private $repository;

    public function __construct(ChangesRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @throws Exception
     */
    public function run()
    {
        $now = new DateTimeImmutable();
        $dateStampDirName = $now->format('Ymd');
        $this->repository->createAppliedFolderByDate($dateStampDirName);

        $processables = $this->repository->getAllPendingChanges();
        $compiledHandle = $this->repository->processChangesByDatestamp($processables, $dateStampDirName);
        $this->repository->writeCompiledToDatabase($compiledHandle);
        $this->repository->unlinkAppliedChanges($processables);
    }
}
