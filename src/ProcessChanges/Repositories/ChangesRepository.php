<?php declare(strict_types=1);

namespace Preskok\ProcessChanges\Repositories;

use Exception;
use PDO;
use PDOException;
use Preskok\ProcessChanges\Exceptions\NoProcessableChangesException;
use Preskok\ProcessChanges\Exceptions\UnreachableResourceException;

class ChangesRepository
{
    const PENDING_FOLDER = BASE_DIR . '/storage/pending/';
    const CHANGE_REGEX = '/change_(\d+)_j.sql/';

    /**
     * @var PDO
     */
    private $db;

    public function __construct(PDO $db)
    {
        $this->db = $db;
    }

    /**
     * @return array
     * @throws Exception
     */
    public function getAllPendingChanges(): array
    {
        $processableFiles = [];
        $files = scandir(self::PENDING_FOLDER);
        foreach ($files as $file) {
            if (preg_match(self::CHANGE_REGEX, $file)) {
                $processableFiles[] = $file;
            }
        }

        if (!count($processableFiles)) {
            throw new NoProcessableChangesException('No SQL files found to process!');
        }

        return $processableFiles;
    }

    public function createAppliedFolderByDate($dateStampDirName): void
    {
        if (!is_dir(BASE_DIR . '/storage/applied/' . $dateStampDirName)) {
            mkdir(BASE_DIR . '/storage/applied/' . $dateStampDirName);
        }
    }

    /**
     * @param array $processables
     * @param string $dateStampDirName
     *
     * @return bool|resource
     *
     * @throws UnreachableResourceException
     */
    public function processChangesByDatestamp(array $processables, string $dateStampDirName)
    {
        $outputPath = $this->getBundledOutputPath($dateStampDirName);

        $outputHandle = fopen($outputPath, 'w+');
        if (!$outputHandle) {
            throw new UnreachableResourceException('Cannot open target file! ' . $outputPath);
        }
        foreach ($processables as $file) {
            $inputPath = $this->getPendingFilePath($file);

            $inputHandler = fopen($inputPath, 'r');
            if (!$inputHandler) {
                throw new UnreachableResourceException("Cannot open file $file");
            }

            while (!feof($inputHandler)) {
                $line = fgets($inputHandler);
                fwrite($outputHandle, $line);
            }

            fwrite($outputHandle, PHP_EOL);
            fclose($inputHandler);
        }

        return $outputHandle;
    }

    /**
     * @param $handle
     *
     * @throws Exception
     */
    public function writeCompiledToDatabase($handle)
    {
        rewind($handle);

        try {
            $this->db->beginTransaction();

            while (!feof($handle)) {
                $line = fgets($handle);
                if ($line) {
                    $this->db->query($line);
                }
            }

            $this->db->commit();
        } catch (PDOException $e) {
            $this->db->rollback();

            throw $e;
        } finally {
            fclose($handle);
        }
    }

    public function unlinkAppliedChanges(array $processables): void
    {
        foreach ($processables as $file) {
            unlink($this->getPendingFilePath($file));
        }
    }

    /**
     * @return bool
     */
    protected function isWindows(): bool
    {
        return strtoupper(substr(PHP_OS, 0, 3)) === 'WIN';
    }

    /**
     * @param string $dateStampDirName
     *
     * @return string
     */
    protected function getBundledOutputPath(string $dateStampDirName): string
    {
        $outputPath = BASE_DIR . '/storage/applied/' . $dateStampDirName . '/applied.sql';
        if ($this->isWindows()) {
            $outputPath = BASE_DIR . '\\storage\\applied\\' . $dateStampDirName . '\\applied.sql';
        }

        return $outputPath;
    }

    /**
     * @param $file
     *
     * @return string
     */
    protected function getPendingFilePath($file): string
    {
        $inputPath = BASE_DIR . '/storage/pending/' . $file;
        if ($this->isWindows()) {
            $inputPath = BASE_DIR . '\\storage\\pending\\' . $file;
        }

        return $inputPath;
    }
}
