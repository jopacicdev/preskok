<?php declare(strict_types=1);

namespace Preskok\ProcessChanges\Commands;

use Exception;
use Preskok\ProcessChanges\Enums\ProcessChangesEnum;
use Preskok\ProcessChanges\Services\ProcessChangesService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ProcessChanges extends Command
{
    protected function configure()
    {
        $this
            ->setName(ProcessChangesEnum::COMMAND_NAME)
            ->setDescription(ProcessChangesEnum::COMMAND_DESCRIPTION);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var ProcessChangesService $processChangesService */
        $processChangesService = $this->getApplication()->getContainer()->get(ProcessChangesService::class);

        $output->write('<info>Processing changes... </info>');
        $processChangesService->run();
        $output->write('<info>Done!</info>' . PHP_EOL);
    }
}
