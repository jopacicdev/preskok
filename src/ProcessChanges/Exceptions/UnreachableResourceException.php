<?php declare(strict_types=1);

namespace Preskok\ProcessChanges\Exceptions;

use Exception;

class UnreachableResourceException extends Exception
{

}
