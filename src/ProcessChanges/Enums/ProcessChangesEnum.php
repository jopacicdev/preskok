<?php declare(strict_types=1);

namespace Preskok\ProcessChanges\Enums;

class ProcessChangesEnum
{
    const COMMAND_NAME = 'ProcessChanges';
    const COMMAND_DESCRIPTION = 'Bundles up pending changes, runs against the database and archives applied.';
}
