<?php declare(strict_types=1);

namespace Preskok\AddRecord\Enums;

class AddRecordEnum
{
    const COMMAND_NAME = 'AddRecord';
    const COMMAND_DESCRIPTION = 'Add record to the database.';

    const VEHICLE_ID = 'VehicleID';
    const INVALID_VEHICLE_ID_EXCEPTION_MESSAGE = 'VehicleID must be valid ID';

    const INHOUSE_SELLER_ID = 'InhouseSellerID';
    const INVALID_INHOUSE_SELLER_ID_EXCEPTION_MESSAGE = 'InhouseSellerID must be valid ID';

    const BUYER_ID = 'BuyerID';
    const INVALID_BUYER_ID_EXCEPTION_MESSAGE = 'BuyerID must be valid ID';

    const MODEL_ID = 'ModelID';
    const INVALID_MODEL_ID_EXCEPTION_MESSAGE = 'ModelID must be valid ID';

    const SALE_DATE = 'SaleDate';
    const INVALID_SALE_DATE_EXCEPTION_MESSAGE = 'SaleDate must be valid date in YYYY-MM-DD format';

    const BUY_DATE = 'BuyDate';
    const INVALID_BUY_DATE_EXCEPTION_MESSAGE = 'BuyDate must be valid date in YYYY-MM-DD format';
}
