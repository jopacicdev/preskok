<?php declare(strict_types=1);

namespace Preskok\AddRecord\Validators;

use Preskok\Application\Contracts\ValidatorInterface;

class DateValidator implements ValidatorInterface
{
    const DATE_FORMAT_REGEX = '/^([0-9]{4})-([0-9]{2})-([0-9]{2})$/';

    public function isValid($value): bool
    {
        return (is_string($value) && preg_match(self::DATE_FORMAT_REGEX, $value));
    }
}