<?php declare(strict_types=1);

namespace Preskok\AddRecord\Validators;

use Preskok\Application\Contracts\ValidatorInterface;

class IdValidator implements ValidatorInterface
{
    public function isValid($value): bool
    {
        return (is_int($value) && $value > 0);
    }
}
