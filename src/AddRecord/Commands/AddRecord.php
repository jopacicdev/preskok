<?php declare(strict_types=1);

namespace Preskok\AddRecord\Commands;

use InvalidArgumentException;
use Preskok\AddRecord\Enums\AddRecordEnum;
use Preskok\AddRecord\Validators\DateValidator;
use Preskok\AddRecord\Validators\IdValidator;
use Preskok\Application\Exceptions\DatabaseErrorException;
use Preskok\Application\Services\RecordService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class AddRecord extends Command
{
    protected function configure()
    {
        $this
            ->setName(AddRecordEnum::COMMAND_NAME)
            ->setDescription(AddRecordEnum::COMMAND_DESCRIPTION)
            ->addArgument(AddRecordEnum::VEHICLE_ID, InputArgument::REQUIRED, AddRecordEnum::VEHICLE_ID)
            ->addArgument(AddRecordEnum::INHOUSE_SELLER_ID, InputArgument::REQUIRED, AddRecordEnum::INHOUSE_SELLER_ID)
            ->addArgument(AddRecordEnum::BUYER_ID, InputArgument::REQUIRED, AddRecordEnum::BUYER_ID)
            ->addArgument(AddRecordEnum::MODEL_ID, InputArgument::REQUIRED, AddRecordEnum::MODEL_ID)
            ->addArgument(AddRecordEnum::SALE_DATE, InputArgument::REQUIRED, AddRecordEnum::SALE_DATE)
            ->addArgument(AddRecordEnum::BUY_DATE, InputArgument::REQUIRED, AddRecordEnum::BUY_DATE);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     *
     * @throws DatabaseErrorException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $enumeratedInputData = [
            AddRecordEnum::VEHICLE_ID => (int)$input->getArgument(AddRecordEnum::VEHICLE_ID),
            AddRecordEnum::INHOUSE_SELLER_ID => (int)$input->getArgument(AddRecordEnum::INHOUSE_SELLER_ID),
            AddRecordEnum::BUYER_ID => (int)$input->getArgument(AddRecordEnum::BUYER_ID),
            AddRecordEnum::MODEL_ID => (int)$input->getArgument(AddRecordEnum::MODEL_ID),
            AddRecordEnum::SALE_DATE => (string)$input->getArgument(AddRecordEnum::SALE_DATE),
            AddRecordEnum::BUY_DATE => (string)$input->getArgument(AddRecordEnum::BUY_DATE),
        ];

        $this->validate($enumeratedInputData);

        $output->write('<info>Adding record to database... </info>');
        /** @var RecordService $recordService */
        $recordService = $this->getApplication()->getContainer()->get(RecordService::class);
        $result = $recordService->add($enumeratedInputData);

        $output->write('<info>Added!</info>' . PHP_EOL);
    }

    /**
     * @param array $record
     *
     * @throws InvalidArgumentException
     */
    private function validate(array $record): void
    {
        /** @var IdValidator $idValidator */
        $idValidator = $this->getApplication()->getContainer()->get(IdValidator::class);
        /** @var DateValidator $dateValidator */
        $dateValidator = $this->getApplication()->getContainer()->get(DateValidator::class);

        if (!(isset($record[AddRecordEnum::VEHICLE_ID]) && $idValidator->isValid($record[AddRecordEnum::VEHICLE_ID]))) {
            throw new InvalidArgumentException(AddRecordEnum::INVALID_VEHICLE_ID_EXCEPTION_MESSAGE);
        }

        if (!(isset($record[AddRecordEnum::INHOUSE_SELLER_ID]) && $idValidator->isValid($record[AddRecordEnum::INHOUSE_SELLER_ID]))) {
            throw new InvalidArgumentException(AddRecordEnum::INVALID_INHOUSE_SELLER_ID_EXCEPTION_MESSAGE);
        }

        if (!(isset($record[AddRecordEnum::BUYER_ID]) && $idValidator->isValid($record[AddRecordEnum::BUYER_ID]))) {
            throw new InvalidArgumentException(AddRecordEnum::INVALID_BUYER_ID_EXCEPTION_MESSAGE);
        }

        if (!(isset($record[AddRecordEnum::MODEL_ID]) && $idValidator->isValid($record[AddRecordEnum::MODEL_ID]))) {
            throw new InvalidArgumentException(AddRecordEnum::INVALID_MODEL_ID_EXCEPTION_MESSAGE);
        }

        if (!(isset($record[AddRecordEnum::SALE_DATE]) && $dateValidator->isValid($record[AddRecordEnum::SALE_DATE]))) {
            throw new InvalidArgumentException(AddRecordEnum::INVALID_SALE_DATE_EXCEPTION_MESSAGE);
        }

        if (!(isset($record[AddRecordEnum::BUY_DATE]) && $dateValidator->isValid($record[AddRecordEnum::BUY_DATE]))) {
            throw new InvalidArgumentException(AddRecordEnum::INVALID_BUY_DATE_EXCEPTION_MESSAGE);
        }
    }
}
