<?php declare(strict_types=1);

namespace Preskok\Report\Enums;

class BestSellingModelPerClientEnum
{
    const COMMAND_NAME = 'BestSellingModelPerClient';
    const COMMAND_DESCRIPTION = 'Get the best selling model per client report.';

    const OUTPUT_TABLE_HEADERS = [
        'Client ID',
        'Client Full Name',
        'Model ID',
        'Model Sales Count',
    ];
}
