<?php declare(strict_types=1);

namespace Preskok\Report\Enums;

class BestSellingModelThreeMonthsPerClientEnum
{
    const COMMAND_NAME = 'BestSellingModelThreeMonthsPerClient';
    const COMMAND_DESCRIPTION = 'Get the best selling model, for 3 months in a row, per client report.';

    const OUTPUT_TABLE_HEADERS = [
        'Model',
        'Begin',
        'End',
        'Sale Count'
    ];
}
