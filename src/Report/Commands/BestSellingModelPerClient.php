<?php declare(strict_types=1);

namespace Preskok\Report\Commands;

use Exception;
use Preskok\Application\Services\ReportsService;
use Preskok\Report\Enums\BestSellingModelPerClientEnum;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class BestSellingModelPerClient extends Command
{
    protected function configure()
    {
        $this
            ->setName(BestSellingModelPerClientEnum::COMMAND_NAME)
            ->setDescription(BestSellingModelPerClientEnum::COMMAND_DESCRIPTION);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     *
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var ReportsService $reportsService */
        $reportsService = $this->getApplication()->getContainer()->get(ReportsService::class);
        $report = $reportsService->getBestSellingModelPerClient();

        if (empty($report)) {
            $output->write('<info>No results.</info>');
        }

        $table = new Table($output);
        $table->setHeaders(BestSellingModelPerClientEnum::OUTPUT_TABLE_HEADERS);
        $table->setRows($report);
        $table->render();
    }
}
