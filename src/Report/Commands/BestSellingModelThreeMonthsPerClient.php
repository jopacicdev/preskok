<?php declare(strict_types=1);

namespace Preskok\Report\Commands;

use Preskok\Application\Exceptions\DatabaseErrorException;
use Preskok\Application\Services\ReportsService;
use Preskok\Report\Enums\BestSellingModelThreeMonthsPerClientEnum;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class BestSellingModelThreeMonthsPerClient extends Command
{
    protected function configure()
    {
        $this
            ->setName(BestSellingModelThreeMonthsPerClientEnum::COMMAND_NAME)
            ->setDescription(BestSellingModelThreeMonthsPerClientEnum::COMMAND_DESCRIPTION);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     *
     * @throws DatabaseErrorException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var ReportsService $reportsService */
        $reportsService = $this->getApplication()->getContainer()->get(ReportsService::class);
        $report = $reportsService->getBestSellingModelThreeMonthsPerClient();

        if (empty($report)) {
            $output->writeln('<info>No results.</info>');
        }

        $table = new Table($output);
        $table->setHeaders(BestSellingModelThreeMonthsPerClientEnum::OUTPUT_TABLE_HEADERS);
        $table->setRows($report);
        $table->render();
    }
}
