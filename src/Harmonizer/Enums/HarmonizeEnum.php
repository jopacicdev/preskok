<?php declare(strict_types=1);

namespace Preskok\Harmonizer\Enums;

class HarmonizeEnum
{
    const COMMAND_NAME = 'Harmonize';
    const COMMAND_DESCRIPTION = 'Get local and remote bucket in perfect harmony.';

    const COMMAND_ARGUMENT_FOLDER = 'folder';
    const COMMAND_ARGUMENT_DESCRIPTION = 'Local folder';
    const INVALID_ARGUMENT_EXCEPTION_MESSAGE = 'The given folder is not valid!';
}
