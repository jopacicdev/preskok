<?php declare(strict_types=1);

namespace Preskok\Harmonizer\Commands;

use Preskok\Harmonizer\Enums\HarmonizeEnum;
use Preskok\Harmonizer\Services\HarmonizeService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Harmonize extends Command
{
    public function configure()
    {
        $this
            ->setName(HarmonizeEnum::COMMAND_NAME)
            ->setDescription(HarmonizeEnum::COMMAND_DESCRIPTION)
            ->addArgument(
                HarmonizeEnum::COMMAND_ARGUMENT_FOLDER,
                InputArgument::REQUIRED,
                HarmonizeEnum::COMMAND_ARGUMENT_DESCRIPTION
            );
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $folder = $input->getArgument(HarmonizeEnum::COMMAND_ARGUMENT_FOLDER);

        $output->write("<info>Running harmonizer... ");
        /** @var HarmonizeService $harmonizeService */
        $harmonizeService = $this->getApplication()->getContainer()->get(HarmonizeService::class);
        $harmonizeService->run($folder);
        $output->write("<info>Done!</info>" . PHP_EOL);
    }
}
