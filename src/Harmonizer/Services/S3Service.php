<?php declare(strict_types=1);

namespace Preskok\Harmonizer\Services;

use Aws\S3\S3Client;
use Preskok\Application\DTO\File;

class S3Service
{
    /**
     * @var S3Client
     */
    private $client;

    public function __construct(S3Client $client)
    {
        $this->client = $client;
    }

    public function upload(string $file, string $contents, string $bucket)
    {
        $this->initBucket($bucket);

        $this->client->putObject([
            'ACL' => 'public-read',
            'Bucket' => $bucket,
            'Key' => $file,
            'Body' => $contents,
        ]);
    }

    public function getRemoteFiles(string $bucket): array
    {
        $result = $this->client->listObjects(
            [
                'Bucket' => $bucket,
            ]
        );

        $objects = [];
        $remoteObjects = (array)$result['Contents'];

        foreach ($remoteObjects as $remoteObject) {
            $objects[] = $remoteObject['Key'];
        }

        return $objects;
    }

    /**
     * @param string $file
     * @param string $bucket
     *
     * @return File
     */
    public function getRemoteFileDTO(string $file, string $bucket): File
    {
        $remoteFile = $this->client->getObject(
            [
                'Bucket' => $bucket,
                'Key' => $file,
            ]
        );
        $contents = (string)$remoteFile['Body'];

        return new File($file, $contents);
    }

    private function initBucket(string $bucket): void
    {
        if (!$this->client->doesBucketExist($bucket)) {
            $this->client->createBucket([
                'Bucket' => $bucket,
            ]);
        };
    }
}
