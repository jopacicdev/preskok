<?php declare(strict_types=1);

namespace Preskok\Harmonizer\Services;

use Preskok\Application\Repositories\LogRepository;
use Preskok\Application\Repositories\StorageRepository;

class HarmonizeService
{
    /**
     * @var StorageRepository
     */
    private $storageRepository;
    /**
     * @var LogRepository
     */
    private $logRepository;
    /**
     * @var S3Service
     */
    private $s3Service;

    public function __construct(
        S3Service $s3Service,
        StorageRepository $storageRepository,
        LogRepository $logRepository
    ) {
        $this->storageRepository = $storageRepository;
        $this->logRepository = $logRepository;
        $this->s3Service = $s3Service;
    }

    /**
     * @inheritdoc
     */
    public function run($directory)
    {
        $syncableFiles = $this->storageRepository->getLocalFiles($directory);
        $localFiles = $this->logRepository->getLocalVersionedFiles();
        $unversionedFiles = array_diff($syncableFiles, $localFiles);
        $this->logRepository->addLocalFiles($unversionedFiles);

        foreach ($unversionedFiles as $unversionedFile) {
            $fileDto = $this->storageRepository->getLocalFileDTO($unversionedFile, $directory);
            $this->s3Service->upload($fileDto->filename, $fileDto->contents, $directory);
            $this->logRepository->setFileToSynced($fileDto->filename);
        }

        $syncableFiles = $this->s3Service->getRemoteFiles($directory);
        $remoteFiles = $this->logRepository->getRemoteVersionedFiles();
        $unversionedFiles = array_diff($syncableFiles, $remoteFiles);
        $this->logRepository->addRemoteFiles($unversionedFiles);

        foreach ($unversionedFiles as $unversionedFile) {
            $fileDto = $this->s3Service->getRemoteFileDTO($unversionedFile, $directory);
            $this->storageRepository->save($fileDto->filename, $fileDto->contents, $directory);
            $this->logRepository->setFileToSynced($fileDto->filename);
        }
    }
}
