<?php declare(strict_types=1);

namespace Preskok\Migration\Repositories;

use PDO;

class MigrationRepository
{
    private $db;

    public function __construct(PDO $db)
    {
        $this->db = $db;
    }

    public function createRecordsTable()
    {
        $query = "
            CREATE TABLE `records` (
              `VehicleID`       int(11) unsigned NOT NULL,
              `InhouseSellerID` int(11) unsigned NOT NULL,
              `BuyerID`         int(11) unsigned NOT NULL,
              `ModelID`         int(11) unsigned NOT NULL,
              `SaleDate`        date NOT NULL,
              `BuyDate`         date NOT NULL,
              PRIMARY KEY (`VehicleID`)
            ) ENGINE = InnoDB;
        ";

        $this->db->exec($query);
    }

    public function createBuyersTable()
    {
        $query = "
            CREATE TABLE `buyers` ( 
              `ID`         INT(11)     NOT NULL    AUTO_INCREMENT , 
              `FirstName`  VARCHAR(50) NOT NULL , 
              `LastName`   VARCHAR(50) NOT NULL , 
              PRIMARY KEY (`ID`)
            ) ENGINE = InnoDB;
        ";

        $this->db->exec($query);
    }

    public function createEntityTable()
    {
        $query = "CREATE TABLE `entity` (
        `id`         int(11)     NOT     NULL,
        `name`       varchar(50) DEFAULT NULL,
        `created_at` datetime    NOT     NULL,
        PRIMARY KEY (`id`)
       ) ENGINE=InnoDB";

        $this->db->exec($query);
    }

    public function createLogTable()
    {
        $query = "CREATE TABLE `log` ( 
        `id`     INT                          NOT NULL    AUTO_INCREMENT, 
        `file`   VARCHAR(100)                 NOT NULL , 
        `status` ENUM('SYNC','AWS','DISK','') NOT NULL , 
        PRIMARY KEY (`id`)
        ) ENGINE = InnoDB;";

        $this->db->exec($query);
    }
}
