<?php declare(strict_types=1);

namespace Preskok\Migration\Enums;

class MigrationEnum
{
    const COMMAND_NAME = 'Migrate';
    const COMMAND_DESCRIPTION = 'Create appropriate DB tables if they don\'t exist';
}
