<?php declare(strict_types=1);

namespace Preskok\Migration\Services;

use Preskok\Migration\Repositories\MigrationRepository;

class MigrationService
{
    /**
     * @var MigrationRepository
     */
    private $repository;

    public function __construct(MigrationRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        $this->repository->createBuyersTable();
        $this->repository->createRecordsTable();
        $this->repository->createEntityTable();
        $this->repository->createLogTable();
    }
}
