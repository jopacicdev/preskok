<?php declare(strict_types=1);

namespace Preskok\Migration\Commands;

use Exception;
use Preskok\Migration\Enums\MigrationEnum;
use Preskok\Migration\Services\MigrationService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Migration extends Command
{
    protected function configure()
    {
        $this
            ->setName(MigrationEnum::COMMAND_NAME)
            ->setDescription(MigrationEnum::COMMAND_DESCRIPTION);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->write('<info>Running migrations... </info>');

        try {
            /** @var MigrationService $migrationService */
            $migrationService = $this->getApplication()->getContainer()->get(MigrationService::class);
            $migrationService->run();
            $output->write('<info>Done!</info>');
        } catch (Exception $e) {
            $output->write('<error>Failed :(</error>' . PHP_EOL);
            $output->writeln('<info>' . $e->getMessage() . '</info>');
        }
    }
}
