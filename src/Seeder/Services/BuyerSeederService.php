<?php declare(strict_types=1);

namespace Preskok\Seeder\Services;

use Preskok\Application\Repositories\BuyerRepository;
use Preskok\Application\Repositories\RecordRepository;

class BuyerSeederService
{
    /**
     * @var BuyerRepository
     */
    private $buyerRepository;
    /**
     * @var RecordRepository
     */
    private $recordRepository;

    public function __construct(BuyerRepository $buyersRepository, RecordRepository $recordsRepository)
    {
        $this->buyerRepository = $buyersRepository;
        $this->recordRepository = $recordsRepository;
    }

    public function run()
    {
        $buyerIdsList = $this->recordRepository->getAggregatedListOfBuyerIds();

        $this->buyerRepository->generateFakeBuyers($buyerIdsList);
    }
}
