<?php declare(strict_types=1);

namespace Preskok\Seeder\Enums;

class BuyerSeederEnum
{
    const COMMAND_NAME = 'BuyerSeeder';
    const COMMAND_DESCRIPTION = 'Seed buyers table with fake buyers';
}
