<?php declare(strict_types=1);

namespace Preskok\Seeder\Commands;

use Preskok\Seeder\Enums\BuyerSeederEnum;
use Preskok\Seeder\Services\BuyerSeederService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class BuyerSeeder extends Command
{
    protected function configure()
    {
        $this
            ->setName(BuyerSeederEnum::COMMAND_NAME)
            ->setDescription(BuyerSeederEnum::COMMAND_DESCRIPTION);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->write('<info>Seeding `buyers` table... </info>');

        /** @var BuyerSeederService $seederService */
        $seederService = $this->getApplication()->getContainer()->get(BuyerSeederService::class);
        $seederService->run();

        $output->write('<info>Done!</info>' . PHP_EOL);
    }
}
