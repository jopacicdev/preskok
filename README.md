# Preskok Cli App

### Requirements
To run this app, you'd need to meet the following criteria:
 * PHP interpreter >=7.1
 * Composer is installed on the system

### Installation
To install the app, clone it inside some directory on the server and run:
```
composer install
```
### Configuration
The app uses `dotenv` for configuration, so in order to setup the app properly, please fill out the `.env` file with keys that could be found in `.env.sample`.

### Running tests
To run the unit tests, please use the `phpunit` binary:
```
./vendor/bin/phpunit
```

### Running commands
This is a CLI app, meaning that you can invoke a command with a single call from your terminal. In order to do that, please run:
```
php app.php <your command>
```

To get a full list of possible commands, just run:
```
php app.php
```

### Migration
To install the database tables, please run the `Migrate` command:
```
php app.php Migrate
```
This command will run and create tables required to run the application. Leverages the PDO connection.

### Import remote data
To import the data from a remote URL, please run the `Import` command:
```
php app.php Import <url>
```
The `url` parameter is REQUIRED.

### Seed buyers
To seed the `buyers` table, please run the `BuyerSeeder` command:
```
php app.php BuyerSeeder
```
It will aggregate buyer IDs from the `records` table, and generate fake names and add them to the `buyers` table.

### Add new record
This commands provides the possibility to add the new record directly from the command line. To do that, run as follows:
```
php app.php AddRecord <VehicleID> <InhouseSellerID> <BuyerID> <ModelID> <SaleDate> <BuyDate>
```
The input is validated as follows:
 * first 4 arguments are considered IDs, so only numeric positive values are accepted. Will be evaluated as integer.
 * last 2 arguments are dates, and only accepted in YYYY-MM-DD format

### Sale reports
There are two commands available for generating reports:
 * BestSellingModelPerClient
 * BestSellingModelThreeMonthsPerClient
```
php app.php BestSellingModelPerClient
php app.php BestSellingModelThreeMonthsPerClient
```
The first one will return the client details, along with Model ID and Model's sales count.
The second one will return the Model ID and 3 month span in which the best sales are achieved, along with the sales count.

### Process Changes
Inside the `storage` folder, there are 2 subfolders: `pending` and `applied`. The goal is to bundle up the SQL files inside the `pending` folder, run them against the database, and store the bundled file inside the `applied` folder, under the correct datestamp dir name.
To run the command:
```
php app.php ProcessChanges
```

### Harmonize local and remote files (AWS S3)
The command is being ran with the `foldername` paramenter, which will look under the `storage` folder for a given `foldername`. It will then harmonize local and remote (AWS S3) storage. The `foldername` corresponds to S3 bucket name.
```
php app.php <foldername>
```

E.g.:
```
php app.php sync
```