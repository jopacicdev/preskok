<?php declare(strict_types=1);

namespace Preskok\AddRecord\Validators;

use PHPUnit\Framework\TestCase;

class IdValidatorTest extends TestCase
{
    /** @var IdValidator */
    private $validator;

    protected function setUp()/* The :void return type declaration that should be here would cause a BC issue */
    {
        $this->validator = new IdValidator();
    }

    /**
     * @dataProvider idsDataProvider
     *
     * @param $id
     * @param $expected
     */
    public function testIsValid($id, bool $expected)
    {
        $this->assertSame($expected, $this->validator->isValid($id));
    }

    public function idsDataProvider()
    {
        return [
            [1, true],
            [32456, true],
            ['1', false],
            [-1, false],
            [0, false],
            [null, false],
            [[], false],
            [new \stdClass(), false],
        ];
    }
}
