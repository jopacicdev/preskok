<?php declare(strict_types=1);

namespace Preskok\AddRecord\Validators;

use PHPUnit\Framework\TestCase;

class DateValidatorTest extends TestCase
{
    /** @var DateValidator */
    private $validator;

    protected function setUp()/* The :void return type declaration that should be here would cause a BC issue */
    {
        $this->validator = new DateValidator();
    }

    /**
     * @dataProvider datesDataProvider
     *
     * @param $date
     * @param $expected
     */
    public function testIsValid(string $date, bool $expected)
    {
        $this->assertSame($expected, $this->validator->isValid($date));
    }

    public function datesDataProvider()
    {
        return [
            ['2018-01-01', true],
            ['2018/01/01', false],
        ];
    }
}
