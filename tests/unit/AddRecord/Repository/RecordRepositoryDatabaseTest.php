<?php declare(strict_types=1);

namespace Preskok\AddRecord\Repository;

use PDO;
use PHPUnit\Framework\TestCase;
use Preskok\Application\DTO\Record;
use Preskok\Application\Repositories\RecordRepository;

class RecordRepositoryDatabaseTest extends TestCase
{
    private $pdo;
    /** @var RecordRepository */
    private $repository;

    protected function setUp()/* The :void return type declaration that should be here would cause a BC issue */
    {
        $this->pdo = $this->initDatabase();
        $this->repository = new RecordRepository($this->pdo);
    }

    public function testAdd()
    {
        $record = new Record(
            999999,
            2,
            3,
            4,
            '2018-01-01',
            '2018-01-01'
        );

        $result = $this->repository->add($record);

        $checkQuery = "SELECT * FROM records WHERE VehicleID = 999999 LIMIT 1";
        $stmt = $this->pdo->prepare($checkQuery);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_COLUMN, 0);

        $cleanupQuery = "DELETE FROM records WHERE VehicleID = 999999";
        $stmt = $this->pdo->prepare($cleanupQuery);
        $stmt->execute();

        $this->assertEquals(1, count($result));
    }

    private function initDatabase()
    {
        /** Mock the local DB, should be moved to XML cfg */
        $dsn = sprintf(
            'mysql:host=%s:%s;dbname=%s',
            'localhost',
            3306,
            'preskok'
        );

        return new PDO($dsn, 'root', '');
    }
}
