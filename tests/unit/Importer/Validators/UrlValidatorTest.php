<?php

namespace Preskok\Importer\Validators;

use PHPUnit\Framework\TestCase;

class UrlValidatorTest extends TestCase
{
    /** @var UrlValidator */
    private $validator;

    protected function setUp()/* The :void return type declaration that should be here would cause a BC issue */
    {
        $this->validator = new UrlValidator();
    }

    /**
     * @dataProvider urlsDataProvider
     * @param string $url
     * @param bool $expected
     */
    public function testIsValid($url, $expected)
    {
        $this->assertSame($expected, $this->validator->isValid($url));
    }

    public function urlsDataProvider()
    {
        return [
            ['http://google.com', true],
            ['https://google.com', true],
            ['https://example.com/some.json', true],
            ['https://example.com/some//file.json', true],
            ['php://example.com/some//file.json', false],
            ['example.com/some//file.json', false],
            ['ssh://example.com/some//file.json', false],
        ];
    }
}
