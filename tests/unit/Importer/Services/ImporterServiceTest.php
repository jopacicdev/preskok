<?php

namespace Preskok\Importer\Services;

use PHPUnit\Framework\TestCase;
use Preskok\Importer\Repositories\ImporterRepository;

class ImporterServiceTest extends TestCase
{
    private $repositoryMock;

    protected function setUp()/* The :void return type declaration that should be here would cause a BC issue */
    {
        $this->repositoryMock = $this->getMockBuilder(ImporterRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
    }

    /**
     * @dataProvider importDataProvider
     *
     * @param string $url
     * @param array $data
     * @param int $count
     */
    public function testImport($url, $data, $count)
    {
        $this->repositoryMock
            ->expects($this->once())
            ->method('fetchRemoteData')
            ->with($url)
            ->will($this->returnValue($data));

        $this->repositoryMock
            ->expects($this->once())
            ->method('import')
            ->with($data);

        $service = new ImporterService($this->repositoryMock);
        $actual = $service->import($url);

        $this->assertEquals($actual, $count);
    }

    public function importDataProvider(): array
    {
        return [
            ['http://some.example.com/file.abc', [1, 2], 2]
        ];
    }
}
