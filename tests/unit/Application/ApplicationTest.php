<?php

namespace Preskok\Application;

use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;

class ApplicationTest extends TestCase
{
    private $app;

    public function setUp()/* The :void return type declaration that should be here would cause a BC issue */
    {
        /** @var ContainerInterface $containerMock */
        $containerMock = $this->getMockBuilder(ContainerInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->app = new Application($containerMock);
    }

    public function testGetContainer()
    {
        $this->assertInstanceOf(ContainerInterface::class, $this->app->getContainer());
    }
}
