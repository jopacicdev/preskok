<?php

namespace Preskok\Application\Repositories;

use PHPUnit\Framework\TestCase;

class RecordRepositoryTest extends TestCase
{
    /**
     * @dataProvider aggregatedListDataProvider
     * @param $return
     * @param $expected
     */
    public function testGetAggregatedListOfBuyerIds($return, $expected)
    {
        $query = "SELECT DISTINCT BuyerID FROM `records`";

        $statementMock = $this->getMockBuilder(\PDOStatement::class)
            ->disableOriginalConstructor()
            ->getMock();
        $statementMock->expects($this->once())
            ->method('execute');
        $statementMock->expects($this->once())
            ->method('fetchAll')
            ->with(7, 0)
            ->will($this->returnValue($return));

        /** @var \PDO $dbMock */
        $dbMock = $this->getMockBuilder(\PDO::class)
            ->disableOriginalConstructor()
            ->getMock();
        $dbMock->expects($this->once())
            ->method('prepare')
            ->with($query)
            ->will($this->returnValue($statementMock));

        $repository = new RecordRepository($dbMock);
        $actual = $repository->getAggregatedListOfBuyerIds();

        $this->assertEquals($expected, $actual);
    }

    public function aggregatedListDataProvider()
    {
        return [
            [
                ["1", "2"],
                [1,2],
            ]
        ];
    }
}

class PDO
{
}

class PDOStatement
{
}
