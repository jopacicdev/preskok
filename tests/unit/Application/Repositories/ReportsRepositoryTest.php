<?php

namespace Preskok\Application\Repositories;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Preskok\Application\Exceptions\DatabaseErrorException;

class ReportsRepositoryTest extends TestCase
{
    /** @var \PDO|MockObject db */
    private $db;
    /** @var ReportsRepository */
    private $repository;

    protected function setUp()/* The :void return type declaration that should be here would cause a BC issue */
    {
        $this->db = $this->getMockBuilder(\PDO::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->repository = new ReportsRepository($this->db);
    }

    public function testGetBestSellingModelPerClient()
    {
        $returnData = [1,2,3];

        $pdoStatementMock = $this->getMockBuilder(\PDOStatement::class)
            ->disableOriginalConstructor()
            ->getMock();

        $pdoStatementMock->expects($this->once())
            ->method('execute')
            ->will($this->returnValue(true));

        $pdoStatementMock->expects($this->once())
            ->method('fetchAll')
            ->with(2)
            ->will($this->returnValue($returnData));

        $this->db
            ->expects($this->once())
            ->method('prepare')
            ->will($this->returnValue($pdoStatementMock));

        $this->assertSame($returnData, $this->repository->getBestSellingModelPerClient());
    }

    public function testGetBestSellingModelPerClientThrowsDatabaseException()
    {
        $this->expectException(DatabaseErrorException::class);

        $returnData = [1,2,3];

        $pdoStatementMock = $this->getMockBuilder(\PDOStatement::class)
            ->disableOriginalConstructor()
            ->getMock();

        $pdoStatementMock->expects($this->once())
            ->method('execute')
            ->will($this->returnValue(false));

        $pdoStatementMock->expects($this->once())
            ->method('errorInfo')
            ->will($this->returnValue($returnData));

        $this->db
            ->expects($this->once())
            ->method('prepare')
            ->will($this->returnValue($pdoStatementMock));

        $this->assertSame($returnData, $this->repository->getBestSellingModelPerClient());
    }

    public function testGetBestSellingModelThreeMonthsPerClient()
    {
        $returnData = [1,2,3];

        $pdoStatementMock = $this->getMockBuilder(\PDOStatement::class)
            ->disableOriginalConstructor()
            ->getMock();

        $pdoStatementMock->expects($this->once())
            ->method('execute')
            ->will($this->returnValue(true));

        $pdoStatementMock->expects($this->once())
            ->method('fetchAll')
            ->with(2)
            ->will($this->returnValue($returnData));

        $this->db
            ->expects($this->once())
            ->method('prepare')
            ->will($this->returnValue($pdoStatementMock));

        $this->assertSame($returnData, $this->repository->getBestSellingModelThreeMonthsPerClient());
    }

    public function testGetBestSellingModelThreeMonthsPerClientThrowsDatabaseException()
    {
        $this->expectException(DatabaseErrorException::class);

        $returnData = [1,2,3];

        $pdoStatementMock = $this->getMockBuilder(\PDOStatement::class)
            ->disableOriginalConstructor()
            ->getMock();

        $pdoStatementMock->expects($this->once())
            ->method('execute')
            ->will($this->returnValue(false));

        $pdoStatementMock->expects($this->once())
            ->method('errorInfo')
            ->will($this->returnValue($returnData));

        $this->db
            ->expects($this->once())
            ->method('prepare')
            ->will($this->returnValue($pdoStatementMock));

        $this->assertSame($returnData, $this->repository->getBestSellingModelThreeMonthsPerClient());
    }
}
