<?php declare(strict_types=1);

use Aws\S3\S3Client;
use Dotenv\Dotenv;
use Preskok\AddRecord\Commands\AddRecord;
use Preskok\AddRecord\Validators\DateValidator;
use Preskok\AddRecord\Validators\IdValidator;
use Preskok\Application\Application;
use Preskok\Application\Repositories\BuyerRepository;
use Preskok\Application\Repositories\LogRepository;
use Preskok\Application\Repositories\RecordRepository;
use Preskok\Application\Repositories\ReportsRepository;
use Preskok\Application\Repositories\StorageRepository;
use Preskok\Application\Services\RecordService;
use Preskok\Application\Services\ReportsService;
use Preskok\Harmonizer\Commands\Harmonize;
use Preskok\Harmonizer\Services\HarmonizeService;
use Preskok\Harmonizer\Services\S3Service;
use Preskok\Importer\Commands\Import;
use Preskok\Importer\Repositories\ImporterRepository;
use Preskok\Importer\Services\ImporterService;
use Preskok\Importer\Validators\UrlValidator;
use Preskok\Migration\Commands\Migration;
use Preskok\Migration\Repositories\MigrationRepository;
use Preskok\Migration\Services\MigrationService;
use Preskok\ProcessChanges\Commands\ProcessChanges;
use Preskok\ProcessChanges\Repositories\ChangesRepository;
use Preskok\ProcessChanges\Services\ProcessChangesService;
use Preskok\Report\Commands\BestSellingModelPerClient;
use Preskok\Report\Commands\BestSellingModelThreeMonthsPerClient;
use Preskok\Seeder\Commands\BuyerSeeder;
use Preskok\Seeder\Services\BuyerSeederService;

require_once __DIR__ . '/../vendor/autoload.php';

$dotenv = new Dotenv(__DIR__ . '/../');
$dotenv->load();

$container = new League\Container\Container();
$container->add('db', function () {
    $dsn = sprintf(
        'mysql:host=%s:%s;dbname=%s',
        getenv('DB_HOST'),
        getenv('DB_PORT'),
        getenv('DB_NAME')
    );

    return new PDO($dsn, getenv('DB_USER'), getenv('DB_PASS'));
});
$container->add('faker', function () {
    return Faker\Factory::create();
});
$container->add(MigrationService::class)->addArgument(MigrationRepository::class);
$container->add(MigrationRepository::class)->addArgument('db');
$container->add(BuyerSeederService::class)->addArguments([BuyerRepository::class, RecordRepository::class]);
$container->add(BuyerRepository::class)->addArguments(['db', 'faker']);
$container->add(RecordRepository::class)->addArgument('db');
$container->add(UrlValidator::class);
$container->add(ImporterService::class)->addArgument(ImporterRepository::class);
$container->add(ImporterRepository::class)->addArgument('db');
$container->add(ReportsService::class)->addArgument(ReportsRepository::class);
$container->add(ReportsRepository::class)->addArgument('db');
$container->add(ProcessChangesService::class)->addArgument(ChangesRepository::class);
$container->add(ChangesRepository::class)->addArgument('db');
$container->add(HarmonizeService::class)->addArguments(
    [
        S3Service::class,
        StorageRepository::class,
        LogRepository::class
    ]
);
$container->add(StorageRepository::class);
$container->add(LogRepository::class)->addArgument('db');
$container->add(S3Service::class)->addArgument('s3client');
$container->add('s3client', function () {
    return new S3Client([
        'version' => 'latest',
        'region' => 'us-east-1'
    ]);
});
$container->add(IdValidator::class);
$container->add(DateValidator::class);
$container->add(RecordService::class)->addArgument(RecordRepository::class);

$app = new Application($container, 'Preskok', '1.0');
$app->add(new Migration());
$app->add(new BuyerSeeder());
$app->add(new Import());
$app->add(new BestSellingModelPerClient());
$app->add(new BestSellingModelThreeMonthsPerClient());
$app->add(new ProcessChanges());
$app->add(new Harmonize());
$app->add(new AddRecord());

return $app;
