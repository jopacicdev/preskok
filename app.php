<?php declare(strict_types=1);

define('BASE_DIR', __DIR__);
$app = require_once __DIR__ . '/bootstrap/app.php';
$app->run();
